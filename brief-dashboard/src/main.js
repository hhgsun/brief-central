import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router/router'
import store from './store/store'
import VueMeta from 'vue-meta'
import TextareaAutosize from 'vue-textarea-autosize'

Vue.config.productionTip = false

Vue.use(VueMeta);
Vue.use(TextareaAutosize);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
