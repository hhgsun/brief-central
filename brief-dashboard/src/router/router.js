import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dasnboard',
    component: Home,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
  },
  {
    path: '/forms',
    name: 'Forms',
    component: () => import(/* webpackChunkName: "forms" */ '../views/Forms.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/add-form',
    name: 'Add Form',
    component: () => import(/* webpackChunkName: "addform" */ '../views/EditForm.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/update-form/:id',
    name: 'Update Form',
    component: () => import(/* webpackChunkName: "addform" */ '../views/EditForm.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/form/:id',
    name: 'Form Detail',
    component: () => import(/* webpackChunkName: "formdetail" */ '../views/FormDetail.vue'),
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'active',
  linkExactActiveClass: "active",
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('isAdmin')) {
      next();
    } else {
      next({ path: '/login' });
    }
  } else {
    if (to.path == '/login' && localStorage.getItem('isAdmin')) {
      next({ path: '/' });
    }
    next();
  }
})

export default router
