import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const API_URL = 'http://thebluered.co.uk/brief-api/index.php';
const UPLOAD_URL = 'http://thebluered.co.uk/brief-api/uploads/';

/* const API_URL = 'http://localhost:5858';
const UPLOAD_URL = 'http://localhost:5858/uploads/'; */

export default new Vuex.Store({
  state: {
    forms: [],
    formsPage: 0,
    formsTotal: 0,
    formsLimit: 50,
    formsDirection: 'DESC',
    formsSortBy: 'id',
    upload_url: UPLOAD_URL,
  },
  mutations: {
    setAllForms(state, data) {
      state.forms = data.forms;
      state.formsPage = data.page;
      state.formsTotal = data.total;
      state.formsLimit = data.limit;
      state.formsDirection = data.direction;
      state.formsSortBy = data.sort_by;
    },
  },
  actions: {
    async loginAdmin({ }, payload) {
      const res = await fetch(`${API_URL}/admin`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payload)
      });
      return res.json();
    },

    async getAllForms({ commit }, params) {
      var page = 1,
        sort_by = localStorage.getItem('sort_by') != 'undefined' && localStorage.getItem('sort_by') != null ? localStorage.getItem('sort_by') : 'id',
        direction = localStorage.getItem('direction') != 'undefined' && localStorage.getItem('direction') != null ? localStorage.getItem('direction') : 'DESC';
      if (params) {
        page = params.page ? params.page : 1;
        sort_by = params.sort_by ? params.sort_by : 'id';
        direction = params.direction ? params.direction : 'DESC';
      }
      const res = await fetch(`${API_URL}/forms?page=${page}&sort_by=${sort_by}&direction=${direction}`);
      const data = await res.json();
      localStorage.setItem('direction', data.direction);
      localStorage.setItem('sort_by', data.sort_by);
      commit('setAllForms', data);
      return data;
    },

    async addForm({ commit, dispatch }, form) {
      const res = await fetch(`${API_URL}/forms/add`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(form)
      });
      const added = await res.json();
      if (added) {
        dispatch('getAllForms');
        dispatch('imageUpload', form.sections);
      }
      return added;
    },


    async updateForm({ commit, dispatch }, form) {
      const res = await fetch(`${API_URL}/forms/${form.id}/update`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(form)
      });
      const updated = await res.json();
      if (updated) {
        dispatch('getAllForms');
        dispatch('imageUpload', form.sections);
      }
      return updated;
    },

    async imageUpload({ }, sections) {
      const formData = new FormData();
      sections.forEach(section => {
        if (section.value.length > 0) {
          section.value.forEach(fileObj => {
            if (fileObj && fileObj.file != null) {
              if (fileObj.file.size != undefined) {
                formData.append('images[]', fileObj.file, fileObj.fileName);
              }
            }
          });
        }
      });
      const res = await fetch(`${API_URL}/forms/image-upload`, {
        method: 'POST',
        body: formData,
      });
      const uploaded = await res.json();
      console.log(uploaded);
      return uploaded;
    },



    async deleteForm({ commit, dispatch }, formId) {
      const res = await fetch(`${API_URL}/forms/${formId}/delete`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      });
      const del = await res.json();
      dispatch('getAllForms');
      return del;
    },
    async getFormAnswers({ }, formId) {
      const res = await fetch(`${API_URL}/forms/${formId}/answers`);
      const answers = await res.json();
      answers.map(answer => answer.value = JSON.parse(answer.value)); // value JSON.parse("[\"val\", \"val\"]") from=>to ['val', 'val']
      answers.sort(function (a, b) { return a.order_number - b.order_number });
      return answers;
    },
    async getFormDetail({ }, formId) {
      const res = await fetch(`${API_URL}/forms/${formId}`);
      const form = await res.json();
      form.sections.map(section => section.value = JSON.parse(section.value));
      form.sections.sort(function (a, b) { return a.order_number - b.order_number });
      return form;
    },
  },
  modules: {
  }
})
